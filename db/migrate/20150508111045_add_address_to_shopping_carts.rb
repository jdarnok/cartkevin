class AddAddressToShoppingCarts < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :address, :string
  end
end
