class AddCityToShoppingCart < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :city, :string
  end
end
