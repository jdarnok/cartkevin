class AddZipToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :zip, :string
  end
end
