class AddVatIdToShoppingCarts < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :vat_id, :string
  end
end
