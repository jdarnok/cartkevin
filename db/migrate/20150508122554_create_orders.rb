class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :stripe_token
      t.string :stripe_email
      t.string :address
      t.string :vat_id
      t.references :user, index: true

      t.timestamps
    end
  end
end
