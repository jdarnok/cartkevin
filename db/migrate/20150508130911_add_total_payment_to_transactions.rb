class AddTotalPaymentToTransactions < ActiveRecord::Migration
  def change
    add_column :orders, :total_payment, :string
  end
end
