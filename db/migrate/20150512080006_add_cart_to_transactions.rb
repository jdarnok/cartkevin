class AddCartToTransactions < ActiveRecord::Migration
  def change
    add_reference :orders, :shopping_cart, index: true
  end
end
