class AddCountryToShoppingCarts < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :country, :string
  end
end
