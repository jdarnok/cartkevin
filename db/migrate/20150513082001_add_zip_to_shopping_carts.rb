class AddZipToShoppingCarts < ActiveRecord::Migration
  def change
    add_column :shopping_carts, :zip, :string
  end
end
