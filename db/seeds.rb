# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
user = CreateAdminService.new.call
Product.delete_all
puts 'CREATED ADMIN USER: ' << user.email
Product.find_or_create_by(name: 'digital copy no.3', price: 5)
Product.find_or_create_by(name: 'digital copy no.4', price: 10)
Product.find_or_create_by(name: 'digital copy no.1', price: 10)
Product.find_or_create_by(name: 'digital copy no.2', price: 10)
