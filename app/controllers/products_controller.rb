class ProductsController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  def show

  end

  def index
    @products = Product.all
  end

  def create
  end

  def update
  end

  def new
  end

  def destroy
  end
end
