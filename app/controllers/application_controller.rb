class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

#methods for devise to redirect you to your checkout_summary_path
  def after_sign_in_path_for(_resource)
    if session[:checkout]
      session[:checkout] = false
      checkout_path
    else
      root_path
    end
  end
  def after_sign_up_path_for(_resource)
    if session[:checkout]
      session[:checkout] = false
      checkout_path
    else
      root_path
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :name
    devise_parameter_sanitizer.for(:account_update) << :name
  end
end
