class OrdersController < ApplicationController
  before_action :authenticate_user!, only: [:index, :show]
  def index
    @orders = current_user.orders.includes(:product)
  end

  def show
    @orders = current_user.orders.includes(:product)
  end

  def edit
  end

  def update
  end

  def delete
  end

  def create
    @shopping_cart = ShoppingCart.find(order_params[:shopping_cart])
    #stripe payment
    Stripe.api_key = ENV['STRIPE_SECRET_KEY']
    charge = Stripe::Charge.create(
      amount: order_params[:total_payment], # amount in cents
      currency: 'usd',
      source: params['stripeToken'],
      description: "charge of user #{current_user.email} for invoice #{order_params[:vat_id]}"
    )
    shopping_cart = ShoppingCart.find(order_params[:shopping_cart])
    product_ids = params[:product_ids]
    #params[:product_ids] can be like "1,2,3," - so you have to extract last ','
    #and split them for "find" method.
    if product_ids.length > 3
      products = Product.find(product_ids[0..-2].split(','))
      #here I'm making Order which is very similar to ShoppingCart, but
      #thanks to that I can generate Invoice that won't be lost in time,
      #... like tears in rain
      #And I can also see what products does one own.
      ActiveRecord::Base.transaction do
        products.each do |product|
          @order = Order.create(order_params.merge(user: current_user,
                                                   shopping_cart: shopping_cart,
                                                   product: product))
          @shopping_cart.shopping_cart_items.each do |item|
            item.update_attribute(:order_id, @order.id)
          end
        end
      end
    else

      product = Product.find(product_ids[0..-2])
      @order = Order.create(order_params.merge(user: current_user,
                                               shopping_cart: shopping_cart,
                                               product: product))
      @shopping_cart.shopping_cart_items.each do |item|
        item.update_attribute(:order_id, @order.id)
      end
    end
    @shopping_cart.update_attribute(:user, current_user)
    session[:shopping_cart_id] = nil
    redirect_to my_products_path, notice: 'Thank you for purchasing our product'
  end

  private

  def order_params
    params.require(:order).permit(:vat_id, :address, :total_payment, :shopping_cart)
end
end
