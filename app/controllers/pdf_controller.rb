class PdfController < ApplicationController
  #Generating PDF
  def generate
    invoice = ShoppingCart.find(params[:id])
    respond_to do |format|
      format.pdf do
            pdf = ReportPdf.new(invoice, current_user)
            send_data pdf.render, filename: 'report.pdf', type: 'application/pdf'
          end
    end
end

  end
