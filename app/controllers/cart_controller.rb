class CartController < ApplicationController
  before_filter :extract_shopping_cart, except: [:show_invoice, :index_invoice]
  before_action :authenticate_user!, only: [:checkout]
  def show
  end

  def index
  end

  def show_invoice
    @invoice = ShoppingCart.find(params[:id])
  end

  def index_invoice
    @invoices = current_user.shopping_carts
  end

  def create
    session[:checkout] = true
    product = Product.find(params[:product_id])
    @shopping_cart.buy_product(product)
    redirect_to shopping_cart_path, notice: "Thank you for purchasing our product"
  end

  def checkout

    flag = false
    @product_ids = ''
    @shopping_cart.shopping_cart_items.each do |item|
      @product_ids << "#{item.item_id},"
    end
    @products = Product.where(id: @product_ids[0...-1]).to_a
    current_user.products.each do |product|
      flag = true if @products.include? product
    end
    if flag == true
      redirect_to products_path, alert: "at least one of the products in shopping cart is already in your possesion or cart was empty"
    else
      @order = Order.new
    end
  end

  def remove
    @product = Product.find(params[:product_id])
    @shopping_cart.remove(@product, 1)
    redirect_to shopping_cart_path
  end

  def add
    @product = Product.find(params[:product_id])
    @shopping_cart.add(@product, @product.price)
    respond_to do |format|
      format.json {head :no_content}
      format.js
    end
  end

  def update_cart
  end

  def list_owned_products
    @shopping_carts = current_user.shopping_carts.includes(:shopping_cart_items => [:item])
  end

  def update
    respond_to do |format|
      session[:country] = Country.find_country_by_alpha2(shopping_cart_params[:country])
      if @shopping_cart.update(shopping_cart_params)
        format.html { redirect_to checkout_path, notice: 'Vat and address was successfully added!' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @shopping_cart.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def extract_shopping_cart
    shopping_cart_id = session[:shopping_cart_id]
    if ShoppingCart.exists?(id: session[:shopping_cart_id])
      @shopping_cart = ShoppingCart.find(shopping_cart_id)
    else
      session[:shopping_cart_id] = nil
      @shopping_cart = ShoppingCart.create
    end
    session[:shopping_cart_id] = @shopping_cart.id
  end

  def shopping_cart_params
    params.require(:shopping_cart).permit(:vat_id, :address, :city, :zip, :country)
 end
end
