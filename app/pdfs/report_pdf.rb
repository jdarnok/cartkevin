class ReportPdf < Prawn::Document
  #PDF prawn generator
  def initialize(invoice, user)
      super()
      #using fonts that can use european letters
      font_path = "#{Rails.root}/lib/assets/fonts/Open_Sans/"
      font_families.update("Open Sans" => {
                             normal: { file: font_path + "OpenSans-Regular.ttf", font: "OpenSans" },
                             italic: { file: font_path + "OpenSans-Italic.ttf", font: "OpenSans-Italic" },
                             bold: { file: font_path + "OpenSans-Bold.ttf", font: "OpenSans-Bold" },
                             bold_italic: { file: font_path + "OpenSans-BoldItalic.ttf", font: "OpenSans-BoldItalic" }
                           })

      @invoice = invoice
      @user = user
      invoice_to
      invoice_from
      items_list
      rest
    end


  def invoice_to
    font "Open Sans"
    define_grid(columns: 5, rows: 8, gutter: 10)

    grid([0,0], [1,1]).bounding_box do
      text  "INVOICE" , size: 18
      text  @invoice.check_vat_id, align: :left
      text "Date: #{Date.today}", align: :left
      move_down 10
      text @user.name
      text @invoice.address.to_s
      text @invoice.city.to_s
      text @invoice.zip.to_s
      text @invoice.city.to_s
    end
  end

  def invoice_from
    grid([0,3.6], [1,4]).bounding_box do

    # Company address
      move_down 10
      text "Awesomeness Ptd Ltd", align: :left
      text "Address", align: :left
      text "Street 1", align: :left
      text "40300 Shah Alam", align: :left
      text "Selangor", align: :left
      text "Tel No: 42", align: :left
      text "Fax No: 42", align: :left
    end
  end

  def rest
    move_down 50

    text "..............................."
    move_down 10
    text "Signature/Company Stamp"

    move_down 10

    bounding_box([bounds.right - 50, bounds.bottom], width: 60, height: 20) do
      pagecount = page_count
      text "Page #{pagecount}"
    end
  end

  def items_list
    text "Details of Invoice", style: :bold_italic
    stroke_horizontal_rule
    @invoice.shopping_cart_items.each do |item|
      item.name
      item.price
    end

    items = [["Name", "Price"]]
    items += @invoice.shopping_cart_items.map {|item| [item.name, item.price.to_s + "€"]}
    table items, header: true,
                 column_widths: { 0 => 450, 1 => 50}, row_colors: ["FFFFFF"] do
      style(columns(3)) {|x| x.align = :right }
    end
    move_down 10
    items = [["Subtotal",@invoice.subtotal.to_s + "€"]]
    items += [["VAT",@invoice.check_country_vat]]
    items += [["Total ",@invoice.total.to_s + "€"]]
    table items, header: true,
                 column_widths: { 0 => 400, 1 => 100} do
                  style(columns(3)) {|x| x.align = :right }
                end
    end
  end
