class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :shopping_cart
  belongs_to :product
  has_many :shopping_cart_items
end
