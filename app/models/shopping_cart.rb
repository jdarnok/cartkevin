class ShoppingCart < ActiveRecord::Base
  acts_as_shopping_cart
  belongs_to :user
  @@eu_countries = %w(AT BE BG CY CZ DE DK EE ES FI FR GB GR HU HR IE IT LT LU LV MT NL PL PT RO SE SI SK)
  @@europe = %w(AD AL AT AX BA BE BG BY CH CZ DE DK EE ES FI FO FR GB GG GI GR HR HU IE IM IS IT JE LI LT LU LV MC MD ME MK MT NL NO PL PT RO RS RU SE SI SJ SK SM UA VA)
  @@no_eu_countries = @@europe - @@eu_countries
  #Overrided method of "acts_like_a_shopping_cart" gem
  def taxes
    if @@no_eu_countries.include? country
      0
    elsif @@europe.include? country || country == 'NL'
      (subtotal * 0.21)
    else
      0
    end
  end
  #generate string for invoice
  def check_country_vat
    if @@no_eu_countries.include? country
      'VAT shifted'
    elsif @@europe.include? country || country == 'NL'
      'VAT 21%'
    else
      'VAT 0%'
    end
  end
  #generate string for invoice
  def check_vat_id
    if country == 'NL'
      vat_id
    elsif @@eu_countries.include? country
      ''
    elsif @@no_eu_countries.include? country
      vat_id
    else
      ''
      end
  end
  #shopping cart methods packed in one for controller to be slim
  def buy_product(product)
    if  self.shopping_cart_items.empty?
      new_item = self.add(product, product.price)
      new_item.update_attributes(name: product.name)
    else
      cart_item = self.shopping_cart_items.find_by(item_id: product.id)
      if cart_item.blank?
        new_item = self.add(product, product.price  )
        new_item.update_attributes(name: product.name  )
      else
        #when user add same product twice, delete one
      self.remove(cart_item, 1) if cart_item.quantity > 1
      end
    end
  end




end
