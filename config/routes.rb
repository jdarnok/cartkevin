Rails.application.routes.draw do



  get 'pdf/generate'

  root to: 'visitors#index'

  get 'cart/show/', to: 'cart#show', as: 'shopping_cart'
  get 'cart/add/:product_id', to: 'cart#create', as: 'add_to_cart'
  get 'cart/remove/:product_id', to: 'cart#remove', as: 'remove_from_cart'
  get 'cart/addproduct/:product_id', to: 'cart#add', as: 'add_product_to_cart'
  get 'cart/checkout/', to: 'cart#checkout', as: 'checkout'
  get 'cart/update_cart/', to: 'cart#update_cart', as: 'update_cart'
  get 'invoices', to:'cart#index_invoice', as: 'invoices'
  get 'invoices/:id', to:'cart#show_invoice', as: 'invoice'
  get 'invoice/generate/:id', to: 'pdf#generate', as: 'generate_pdf'
  get 'my_products', to: 'cart#list_owned_products', as: 'my_products'
  devise_for :users
  resources :cart
  resources :orders
  resources :products
  resources :users
end
